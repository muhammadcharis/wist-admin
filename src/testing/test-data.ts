import { useEffect, useState } from 'react';

export const testData = {
  users: [
    {
      id: 'KV4Lv9yUHtNVB42V0ZrFf',
      createdAt: 1645628972465,
      email: 'user1@jatis.com',
      password: 'password',
      organizationId: 'amYXmIyT9mD9GyO6CCr',
    },
  ],
  planPackages: [
    {
      id: 'amYXmIyT9mD9GyO6CC1',
      name: 'Paket Harian',
      description:
        'Totam alias fuga enim esse ullam sit. Nisi animi ut at voluptatem odit nam ea',
      period: 'daily',
      threshold: 10,
      charge: 10000,
      created: new Date().toISOString(),
      status: 1,
    },
    {
      id: 'amYXmIyT9mD9GyO6CC2',
      name: 'Paket Mingguan',
      description:
        'Totam alias fuga enim esse ullam sit. Nisi animi ut at voluptatem odit nam ea',
      period: 'weekly',
      threshold: 10,
      charge: 100000,
      created: new Date().toISOString(),
      status: 0,
    },
    {
      id: 'amYXmIyT9mD9GyO6CC3',
      name: 'Paket Bulanan',
      description:
        'Totam alias fuga enim esse ullam sit. Nisi animi ut at voluptatem odit nam ea',
      period: 'monthly',
      threshold: 10,
      charge: 210000,
      created: new Date().toISOString(),
      status: 1,
    },
  ],
};

const delayedFn =
  <T, A extends any[]>(fn: (...args: A) => T, ms: number) =>
  (...args: A) => {
    return new Promise<T>((resolve) =>
      setTimeout(() => resolve(fn(...args)), ms),
    );
  };

export const getUser = delayedFn(() => testData.users[0], 0);

export const getPlanPackages = delayedFn(() => testData.planPackages, 300);

export const getPlanPackage = delayedFn(
  (id: string) => testData.planPackages.filter((p) => p.id === id),
  300,
);

const useTestData = <T>(promise: Promise<T>) => {
  const [testData, setTestData] = useState<T | null>(null);

  useEffect(() => {
    if (!testData) {
      promise.then(setTestData);
    }
  }, [promise, testData]);

  return { data: testData, isLoading: !testData };
};

export const useUser = () => useTestData(getUser());

export const usePlanPackages = () => useTestData(getPlanPackages());

export const usePlanPackage = (id: string) => useTestData(getPlanPackage(id));
