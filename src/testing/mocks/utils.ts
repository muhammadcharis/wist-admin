import { RestRequest } from 'msw';

import jwt_decode from 'jwt-decode';

import { IS_TEST } from '@/configs/constants';

import { testData } from '../test-data';

import { db } from './db';

const AUTH_TOKEN = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9';
const AUTH_HEADER = 'Authorization';

type AuthUser = {
  email: string;
  organizationId: string;
};

export const AUTH_COOKIE = 'auth-token';

const sanitizeUser = (user: any): AuthUser => {
  const sanitizedUser = { ...user };
  delete sanitizedUser.password;
  return sanitizedUser;
};

export const getUser = () => sanitizeUser(testData.users[0]);

// returns the user object and auth token if the provided credentials are valid
export const authenticate = ({
  email,
  password,
}: {
  email: string;
  password: string;
}) => {
  const user = db.user.findFirst({
    where: {
      email: {
        equals: email,
      },
    },
  });

  if (user?.password === password) {
    const sanitizedUser = sanitizeUser(user);
    const encodedToken = AUTH_TOKEN;
    return { user: sanitizedUser, jwt: encodedToken };
  }

  throw new Error('Invalid username or password');
};

// extract the token and return the user if exists
export const requireAuth = ({
  req,
  shouldThrow,
}: {
  req: RestRequest;
  shouldThrow?: boolean;
}): { user: AuthUser | null; noAuth?: boolean } => {
  if (IS_TEST) {
    return { user: getUser() };
  } else {
    const authToken = req.headers.get(AUTH_HEADER);

    if (!authToken || (authToken as string).search('Bearer') === -1) {
      if (shouldThrow) {
        throw new Error('No authorization token provided!');
      }
      return { user: null, noAuth: true };
    }

    const token = authToken.replace('Bearer ', '');
    const decodeJwt: object = jwt_decode(token);

    return { user: { ...getUser(), ...decodeJwt } as AuthUser };
  }
};
