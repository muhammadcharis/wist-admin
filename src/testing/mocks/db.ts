import { factory, primaryKey } from '@mswjs/data';
import { uid } from '../lib/uid';

const models = {
  user: {
    id: primaryKey(uid),
    createdAt: Date.now,
    email: String,
    password: String,
    organizationId: String,
  },
  planPackage: {
    id: primaryKey(uid),
    name: String,
    description: String,
    period: String,
    threshold: Number,
    charge: Number,
    created: String,
    status: Number,
  },
};

export const db = factory(models);
