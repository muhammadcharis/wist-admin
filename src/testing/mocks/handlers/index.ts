import { rest } from 'msw';

import { API_URL } from '@/configs/constants';

import { authHandlers } from './auth';
import { planPackagesHandlers } from './planPackages';

export const handlers = [
  ...authHandlers,
  ...planPackagesHandlers,
  rest.get(`${API_URL}/healthcheck`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json({ healthy: true }));
  }),
];
