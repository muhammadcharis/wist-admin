import { rest } from 'msw';

import { API_URL } from '@/configs/constants';

import { db } from '../db';
import { requireAuth } from '../utils';
import { uid } from '@/testing/lib/uid';

const getPlanPackagesHandler = rest.get(
  `${API_URL}/admin/plan`,
  async (req, res, ctx) => {
    // protect with auth
    const { noAuth } = requireAuth({ req });
    if (noAuth) {
      return res(
        ctx.delay(300),
        ctx.status(401),
        ctx.json({ message: 'Not authorized' }),
      );
    }

    const planPackages = db.planPackage.getAll();

    return res(
      ctx.delay(300),
      ctx.status(200),
      ctx.json({ data: { records: planPackages } }),
    );
  },
);

const getPlanPackageHandler = rest.get(
  `${API_URL}/admin/plan/:planPackageId`,
  async (req, res, ctx) => {
    // protect with auth
    const { noAuth } = requireAuth({ req });
    if (noAuth) {
      return res(
        ctx.delay(300),
        ctx.status(401),
        ctx.json({ message: 'Not authorized' }),
      );
    }

    const id = req.params.planPackageId as string;

    const planPackage = db.planPackage.findFirst({
      where: {
        id: {
          equals: id,
        },
      },
    });

    if (!planPackage) {
      return res(
        ctx.delay(300),
        ctx.status(404),
        ctx.json({ message: 'Not found!' }),
      );
    }

    return res(
      ctx.delay(300),
      ctx.status(200),
      ctx.json({ data: planPackage }),
    );
  },
);

const createPlanPackageHandler = rest.post(
  `${API_URL}/admin/plan`,
  async (req, res, ctx) => {
    // protect with auth
    const { noAuth } = requireAuth({ req });
    if (noAuth) {
      return res(
        ctx.delay(300),
        ctx.status(401),
        ctx.json({ message: 'Not authorized' }),
      );
    }

    const hasPayload = Boolean((req as any)._body.byteLength);

    if (!hasPayload) {
      return res(
        ctx.delay(300),
        ctx.status(402),
        ctx.json({ message: 'Payload !required!' }),
      );
    }
    const payload = await req.json();

    const planPackage = db.planPackage.create({
      ...payload,
      id: uid(),
    });

    return res(ctx.delay(300), ctx.status(200), ctx.json(planPackage));
  },
);

const updatePlanPackageHandler = rest.put(
  `${API_URL}/admin/plan/:planPackageId`,
  async (req, res, ctx) => {
    // protect with auth
    const { noAuth } = requireAuth({ req });
    if (noAuth) {
      return res(
        ctx.delay(300),
        ctx.status(401),
        ctx.json({ message: 'Not authorized' }),
      );
    }

    const id = req.params.planPackageId as string;
    const currentPlanPackage = db.planPackage.findFirst({
      where: {
        id: {
          equals: id,
        },
      },
    });

    if (!currentPlanPackage) {
      return res(
        ctx.delay(300),
        ctx.status(404),
        ctx.json({ message: 'Not found!' }),
      );
    }

    const hasPayload = Boolean((req as any)._body.byteLength);

    if (!hasPayload) {
      return res(
        ctx.delay(300),
        ctx.status(402),
        ctx.json({ message: 'Payload !required!' }),
      );
    }
    const payload = await req.json();

    const planPackage = db.planPackage.update({
      where: {
        id: {
          equals: id,
        },
      },
      data: payload,
    });

    return res(ctx.delay(300), ctx.status(200), ctx.json(planPackage));
  },
);

export const planPackagesHandlers = [
  getPlanPackageHandler,
  getPlanPackagesHandler,
  createPlanPackageHandler,
  updatePlanPackageHandler,
];
