import React from 'react';
import type { NextPage } from 'next';
import { Container, SimpleGrid, Box, Text, MediaQuery } from '@mantine/core';
import Icons from '@/components/commons/icons';
import { useLogin } from '@/hooks/api/auth/use-login';
import LoginForm from '@/components/features/login/login-form';
import useRedirect from '@/hooks/libs/use-redirect';
import { useSelector } from 'react-redux';
import { getAccessToken } from '@/stores/features/auth/slice';

const Login: NextPage = () => {
  const accessToken = useSelector(getAccessToken);
  useRedirect({
    toUrl: '/',
    condition: !!accessToken === true,
  });

  const { mutate: submitLogin, isLoading } = useLogin();

  return (
    <Container size="xl">
      <Box
        sx={(_) => ({
          minHeight: '100vh',
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
        })}
      >
        <SimpleGrid
          style={{ width: '100%' }}
          breakpoints={[
            { minWidth: 1200, cols: 2, spacing: 'sm' },
            { maxWidth: 576, cols: 1, spacing: 'sm' },
          ]}
        >
          <MediaQuery smallerThan="lg" styles={{ display: 'none' }}>
            <Box
              sx={(_) => ({
                minWidth: '611px',
                maxWidth: '611px',
                minHeight: '546px',
                maxHeight: '546px',
                borderRadius: '15px',
                padding: '22px 55px 23px',
                background:
                  'linear-gradient(270deg, #99AAE6 12.92%, #29DDDD 87.08%)',
              })}
            >
              <Icons type="bgLogin" width="501px" height="501px" />
            </Box>
          </MediaQuery>

          <Box
            sx={(_) => ({
              display: 'grid',
              alignItems: 'center',
              justifyContent: 'center',
            })}
          >
            <Container
              style={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                flexDirection: 'column',
                gap: 49,
              }}
            >
              <SimpleGrid cols={1} verticalSpacing="md">
                <Text fw={700} lh={'24px'} fz={{ xs: 'md', lg: 32 }}>
                  Welcome to Our Application
                </Text>
                <Text fw={700} lh={'24px'} fz={14} color={'#B4B4B4'}>
                  Please Login to use this platform
                </Text>
              </SimpleGrid>

              <LoginForm isLoading={isLoading} onSubmit={submitLogin} />
            </Container>
          </Box>
        </SimpleGrid>
      </Box>
    </Container>
  );
};

export default Login;
