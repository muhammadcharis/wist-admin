import React from 'react';
import AppLayout from '@/components/layouts/app-layout';
import type { NextPage } from 'next';

const CekStok: NextPage = () => {
  return (
    <AppLayout title="Cek Stok" pageTitle="Cek Stok">
      Cek Stok
    </AppLayout>
  );
};

export default CekStok;
