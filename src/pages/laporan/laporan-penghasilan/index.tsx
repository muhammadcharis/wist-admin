import React from 'react';
import AppLayout from '@/components/layouts/app-layout';
import type { NextPage } from 'next';

const LaporanPenghasilan: NextPage = () => {
  return (
    <AppLayout title="Laporan Penghasilan" pageTitle="Laporan Penghasilan">
      Laporan Penghasilan
    </AppLayout>
  );
};

export default LaporanPenghasilan;
