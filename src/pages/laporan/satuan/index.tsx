import React from 'react';
import AppLayout from '@/components/layouts/app-layout';
import type { NextPage } from 'next';

const Uom: NextPage = () => {
  return (
    <AppLayout title="Uom" pageTitle="Uom">
      Uom List
    </AppLayout>
  );
};

export default Uom;
