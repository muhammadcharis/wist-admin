import React from 'react';
import AppLayout from '@/components/layouts/app-layout';
import type { NextPage } from 'next';

const Invoice: NextPage = () => {
  return (
    <AppLayout title="Invoice" pageTitle="Invoice">
      Invoice
    </AppLayout>
  );
};

export default Invoice;
