import React from 'react';
import AppLayout from '@/components/layouts/app-layout';
import type { NextPage } from 'next';

const Receipt: NextPage = () => {
  return (
    <AppLayout title="Requisition" pageTitle="Requisition">
      Requisition
    </AppLayout>
  );
};

export default Receipt;
