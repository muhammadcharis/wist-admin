import React from 'react';
import AppLayout from '@/components/layouts/app-layout';
import type { NextPage } from 'next';

const BusinessPartner: NextPage = () => {
  return (
    <AppLayout title="Business Partner" pageTitle="Business Partner">
      Business Partner
    </AppLayout>
  );
};

export default BusinessPartner;
