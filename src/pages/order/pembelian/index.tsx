import React from 'react';
import AppLayout from '@/components/layouts/app-layout';
import type { NextPage } from 'next';

const Order: NextPage = () => {
  return (
    <AppLayout title="Order" pageTitle="Order">
      Order
    </AppLayout>
  );
};

export default Order;
