import React from 'react';
import AppLayout from '@/components/layouts/app-layout';
import type { NextPage } from 'next';

const Payment: NextPage = () => {
  return (
    <AppLayout title="Payment" pageTitle="Payment">
      Payment
    </AppLayout>
  );
};

export default Payment;
