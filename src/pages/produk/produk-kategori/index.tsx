import React from 'react';
import AppLayout from '@/components/layouts/app-layout';
import type { NextPage } from 'next';
import ProdukKategoriTabel from '@/components/features/produk-kategori/produk-ketegori-tabel';
import { useGetProductCategories } from '@/hooks/api/produk-kategori/use-get-product-category';

const ProducCategory: NextPage = () => {
  const { data: productCategories, isLoading } = useGetProductCategories();

  return (
    <AppLayout title="Product Category" pageTitle="Product Category">
      <ProdukKategoriTabel
        isLoading={isLoading}
        data={productCategories ?? []}
      />
    </AppLayout>
  );
};

export default ProducCategory;
