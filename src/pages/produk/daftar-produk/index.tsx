import React from 'react';
import AppLayout from '@/components/layouts/app-layout';
import type { NextPage } from 'next';

const ProductList: NextPage = () => {
  return (
    <AppLayout title="Product List" pageTitle="Product List">
      Product List
    </AppLayout>
  );
};

export default ProductList;
