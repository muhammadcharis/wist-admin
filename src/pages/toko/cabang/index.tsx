import React from 'react';
import AppLayout from '@/components/layouts/app-layout';
import type { NextPage } from 'next';

const Cabang: NextPage = () => {
  return (
    <AppLayout title="Tipe Pembayaran" pageTitle="Tipe Pembayaran">
      Tipe Pembayaran
    </AppLayout>
  );
};

export default Cabang;
