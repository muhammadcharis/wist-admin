import React from 'react';
import AppLayout from '@/components/layouts/app-layout';
import type { NextPage } from 'next';

const Gudang: NextPage = () => {
  return (
    <AppLayout title="Gudang" pageTitle="Gudang">
      Gudang
    </AppLayout>
  );
};

export default Gudang;
