import React from 'react';
import AppLayout from '@/components/layouts/app-layout';
import type { NextPage } from 'next';

const Home: NextPage = () => {
  return (
    <AppLayout title="Dashboard" pageTitle="Dashboard">
      Hello Word
    </AppLayout>
  );
};

export default Home;
