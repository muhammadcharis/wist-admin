import React from 'react';
import Head from 'next/head';
import { AppProps } from 'next/app';
import { PersistGate } from 'redux-persist/integration/react';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { ReactQueryDevtools } from '@tanstack/react-query-devtools';
import { MantineProvider, createEmotionCache } from '@mantine/core';
import { Provider } from 'react-redux';
import { defaultQueryFn } from '@/services/fetcher/axios';
import { store, persistor } from '@/stores/store';
import { NotificationsProvider } from '@mantine/notifications';
import dynamic from 'next/dynamic';
import { API_MOCKING } from '@/configs/constants';
import { MSWWrapperProps } from '@/testing/lib/msw';
import { IS_DEVELOPMENT } from '@/configs/constants';
import configs from '@/configs';

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      queryFn: defaultQueryFn,
    },
  },
});

const myCache = createEmotionCache({ key: 'mantine' });

const MSWWrapper = dynamic<MSWWrapperProps>(() =>
  import('@/testing/lib/msw').then(({ MSWWrapper }) => MSWWrapper),
);

export default function App({ Component, pageProps }: AppProps) {
  const pageContent = API_MOCKING ? (
    <MSWWrapper>
      <Component {...pageProps} />
    </MSWWrapper>
  ) : (
    <Component {...pageProps} />
  );

  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <QueryClientProvider client={queryClient}>
          <ReactQueryDevtools
            initialIsOpen={
              IS_DEVELOPMENT && configs.env.reactQueryDevTools ? true : false
            }
          />
          <MantineProvider
            emotionCache={myCache}
            withGlobalStyles
            withNormalizeCSS
            theme={{
              fontFamily: 'Plus Jakarta Sans, sans-serif',
              colors: {
                brand: [
                  '#FFFFFF',
                  '#FDFEFE',
                  '#DDE5F0',
                  '#BCCEE7',
                  '#99B8E4',
                  '#71A3E9',
                  '#448EF7',
                  '#3B7FE1',
                  '#3573CA',
                  '#3D69A9',
                ],
              },
              primaryColor: 'brand',
            }}
          >
            <NotificationsProvider
              position="top-right"
              styles={{ padding: 50 }}
            >
              <Head>
                <meta charSet="utf-8" />
                <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
                <meta
                  name="viewport"
                  content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no"
                />
                <meta name="description" content="Description" />
                <meta name="keywords" content="Keywords" />
                <title>WIST</title>

                <link
                  href="/icons/favicon-16x16.png"
                  rel="icon"
                  type="image/png"
                  sizes="16x16"
                />
                <link
                  href="/icons/favicon-32x32.png"
                  rel="icon"
                  type="image/png"
                  sizes="32x32"
                />
                <link
                  rel="apple-touch-icon"
                  href="/icons/apple-touch-icon.png"
                ></link>
                <meta name="theme-color" content="#317EFB" />
              </Head>
              {pageContent}
            </NotificationsProvider>
          </MantineProvider>
        </QueryClientProvider>
      </PersistGate>
    </Provider>
  );
}
