import React from 'react';
import AppLayout from '@/components/layouts/app-layout';
import type { NextPage } from 'next';

const Dokter: NextPage = () => {
  return (
    <AppLayout title="Patient" pageTitle="Patient">
      dokter
    </AppLayout>
  );
};

export default Dokter;
