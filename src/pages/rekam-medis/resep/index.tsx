import React from 'react';
import AppLayout from '@/components/layouts/app-layout';
import type { NextPage } from 'next';

const Resep: NextPage = () => {
  return (
    <AppLayout title="Resep" pageTitle="Resep">
      Resep
    </AppLayout>
  );
};

export default Resep;
