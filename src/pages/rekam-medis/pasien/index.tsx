import React from 'react';
import AppLayout from '@/components/layouts/app-layout';
import type { NextPage } from 'next';

const Pasien: NextPage = () => {
  return (
    <AppLayout title="Pasien" pageTitle="Pasien">
      Pasien
    </AppLayout>
  );
};

export default Pasien;
