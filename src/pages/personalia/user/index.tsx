import React from 'react';
import AppLayout from '@/components/layouts/app-layout';
import type { NextPage } from 'next';

const User: NextPage = () => {
  return (
    <AppLayout title="User" pageTitle="User">
      User
    </AppLayout>
  );
};

export default User;
