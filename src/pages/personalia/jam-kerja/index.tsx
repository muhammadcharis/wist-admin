import React from 'react';
import AppLayout from '@/components/layouts/app-layout';
import type { NextPage } from 'next';

const JamKerja: NextPage = () => {
  return (
    <AppLayout title="Jam Kerja" pageTitle="Jam Kerja">
      Jam Kerja
    </AppLayout>
  );
};

export default JamKerja;
