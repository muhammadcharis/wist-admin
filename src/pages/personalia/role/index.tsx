import React from 'react';
import AppLayout from '@/components/layouts/app-layout';
import type { NextPage } from 'next';

const Role: NextPage = () => {
  return (
    <AppLayout title="Role" pageTitle="Role">
      Role
    </AppLayout>
  );
};

export default Role;
