import React from 'react';
import AppLayout from '@/components/layouts/app-layout';
import type { NextPage } from 'next';

const Karyawan: NextPage = () => {
  return (
    <AppLayout title="Karyawan" pageTitle="Karyawan">
      Karyawan
    </AppLayout>
  );
};

export default Karyawan;
