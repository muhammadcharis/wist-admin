import React from 'react';
import { SimpleGrid, TextInput, PasswordInput, Button } from '@mantine/core';
import { IconUser, IconLock } from '@tabler/icons';
import { useForm } from '@mantine/form';
import { LoginFormPropsRequest } from '@/hooks/api/auth/use-login';

type LoginFormProps = {
  isLoading: boolean;
  onSubmit(data: LoginFormPropsRequest): void;
};

const LoginForm = ({ isLoading, onSubmit }: LoginFormProps) => {
  const formLogin = useForm({
    initialValues: {
      username: '',
      password: '',
    },
    validate: (values) => ({
      username: values.username === '' ? 'Username is required' : null,
      password: values.password === '' ? 'Password is required' : null,
    }),
  });

  return (
    <form
      style={{
        width: '100%',
        display: 'grid',
        gap: 45,
      }}
      onSubmit={formLogin.onSubmit((values) => {
        onSubmit(values);
      })}
    >
      <SimpleGrid
        cols={1}
        verticalSpacing="md"
        style={{
          width: '100%',
        }}
      >
        <TextInput
          icon={<IconUser />}
          placeholder="Username"
          radius="md"
          height={40}
          {...formLogin.getInputProps('username')}
        />
        <PasswordInput
          height={40}
          icon={<IconLock />}
          placeholder="Password"
          radius="md"
          {...formLogin.getInputProps('password')}
        />
      </SimpleGrid>

      <Button
        type="submit"
        variant="gradient"
        fullWidth
        loading={isLoading}
        gradient={{ from: '#99AAE6', to: '#29DDDD' }}
      >
        SIGN IN
      </Button>
    </form>
  );
};

export default LoginForm;
