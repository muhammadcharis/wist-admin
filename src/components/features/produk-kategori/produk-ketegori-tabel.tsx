import React, { ChangeEvent } from 'react';
import { Container, Grid, SimpleGrid, Switch, Button } from '@mantine/core';
import { IconPencil, IconTrash } from '@tabler/icons';
import Table from '@/components/commons/table';
import { TableCellType } from '@/types/table';
import { ProductCategoryItem } from '@/types/produk-kategori';

type ProdukKategoriTabelProps = {
  data: ProductCategoryItem[];
  isLoading: boolean;
  //onClickEdit: (d: PlanPackageItem) => void;
  //onClickDelete: (id: string) => void;
};

const headerList = [
  { key: 'no', title: 'No' },
  { key: 'name', title: 'Name' },
  { key: 'description', title: 'Description' },
  { key: 'action', title: 'Action' },
];

const ProdukKategoriTabel = ({
  data,
  isLoading,
}: //onClickEdit,
//onClickDelete,
ProdukKategoriTabelProps) => {
  return (
    <Container px={0} size={'xl'}>
      <SimpleGrid cols={1} breakpoints={[{ maxWidth: 'xl', cols: 1 }]}>
        <Grid gutter="md">
          <Grid.Col span={12}>
            <Table
              isLoading={isLoading}
              headerList={headerList}
              onSearch={(e: ChangeEvent<HTMLInputElement>) =>
                console.warn(e.target.value)
              }
              dataList={data.map((item: ProductCategoryItem, index: number) => {
                const result: TableCellType[] = [];

                result.push({
                  cell: index + 1,
                });

                result.push({
                  cell: item.name,
                });

                result.push({
                  cell: item.description,
                });

                // result.push({
                //   cell: <EditButton onClick={() => onClickEdit(data)} />,
                // });

                return result;
              })}
            />
          </Grid.Col>
        </Grid>
      </SimpleGrid>
    </Container>
  );
};

export default ProdukKategoriTabel;
