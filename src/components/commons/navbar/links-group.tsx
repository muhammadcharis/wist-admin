import {
  Group,
  Box,
  Collapse,
  Text,
  UnstyledButton,
  createStyles,
} from '@mantine/core';
import { IconChevronLeft, IconChevronRight } from '@tabler/icons';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { FormEvent, useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';

import Icons, { TypeIconsProps } from '@/components/commons/icons';
import useWindowSize, { WindowType } from '@/hooks/libs/use-window-size';
import { toggleSidebar } from '@/stores/features/sidebar/slice';
import { Menu } from '@/types/login';

const useStyles = createStyles((theme, sidebarOpen: boolean) => ({
  control: {
    fontWeight: 500,
    display: 'block',
    width: '100%',
    padding: `${theme.spacing.xs}px ${theme.spacing.xs}px`,
    color: theme.black,
    fontSize: theme.fontSizes.sm,

    '&:hover': {
      fontWeight: 700,
      color: theme.colorScheme === 'dark' ? theme.white : theme.black,
    },
  },

  group: {
    justifyContent: sidebarOpen ? 'left' : 'center',

    ['@media (max-width: 767px)']: {
      justifyContent: 'left',
    },
  },

  icon: {
    background: 'transparent',
    border: 0,
  },

  linkParent: {
    textDecoration: 'none !important',
  },

  link: {
    fontWeight: 500,
    display: 'block',
    textDecoration: 'none !important',
    padding: `${theme.spacing.md}px ${theme.spacing.xs}px ${theme.spacing.md}px ${theme.spacing.xl}px`,
    fontSize: theme.fontSizes.sm,
    color: theme.colorScheme === 'dark' ? theme.colors.dark : '#FFFFFF',

    '&:hover': {
      fontWeight: 700,
      color: theme.colorScheme === 'dark' ? theme.black : theme.white,
    },

    '&.active': {
      backgroundColor: theme.white,
      color: '#0f0f0f',
    },
  },

  chevron: {
    transition: 'transform 200ms ease',
    display: sidebarOpen ? 'block' : 'none',
    marginLeft: 'auto',

    ['@media (max-width: 767px)']: {
      display: 'block !important',
    },
  },

  collapse: {
    backgroundColor: theme.white,
  },

  text: {
    '&.close': {
      display: 'none',
    },

    ['@media (max-width: 767px)']: {
      display: 'block !important',
    },
  },
}));

interface LinksGroupProps {
  menuId: number | undefined;
  label: string;
  initiallyOpened?: boolean;
  link?: string;
  subMenus?: Menu[];
  sidebarOpen?: boolean;
  handleToggleSidebar: (f: FormEvent) => void;
}

const LinksGroup = ({
  menuId,
  label,
  initiallyOpened,
  subMenus,
  link,
  sidebarOpen,
  handleToggleSidebar,
}: LinksGroupProps) => {
  const { classes, theme } = useStyles(sidebarOpen || false);
  const dispatch = useDispatch();
  const size: WindowType = useWindowSize();
  const router = useRouter();
  const colorLink = {
    default: '#000000',
    active: '#000000',
  };

  const [opened, setOpened] = useState(initiallyOpened || false);

  const hasLinks = Array.isArray(subMenus);
  const disableHandleOpen = size.width > theme.breakpoints.xs;
  const ChevronIcon = theme.dir === 'ltr' ? IconChevronRight : IconChevronLeft;

  const children = (hasLinks ? subMenus : []).map((subMenu, index) => (
    <Link
      className={`${classes.link} ${
        router.pathname === subMenu.urlPath ? 'active' : ''
      }`}
      key={index}
      href={subMenu.urlPath}
    >
      <Box sx={{ display: 'flex', alignItems: 'center' }}>
        <Text
          ml="md"
          size="sm"
          color={
            router.pathname.includes(subMenu.urlPath as string)
              ? colorLink.active
              : colorLink.default
          }
          fw={router.pathname.includes(subMenu.urlPath as string) ? 700 : 400}
          className={`${classes.text} ${!sidebarOpen ? 'close' : ''}`}
        >
          {subMenu.name}
        </Text>
      </Box>
    </Link>
  ));

  const navItem = (hasLinks?: boolean) => (
    <UnstyledButton
      onClick={(event: FormEvent) => hasLinks && handleToggleCollapse(event)}
      className={classes.control}
      p="md"
    >
      <Group position="apart" spacing={0} className={classes.group}>
        <Box
          sx={{
            display: 'flex',
            alignItems: 'center',
          }}
        >
          <Text
            ml="md"
            size="sm"
            color={
              router.pathname.includes(link as string)
                ? colorLink.active
                : colorLink.default
            }
            fw={router.pathname.includes(link as string) ? 700 : 400}
            className={`${classes.text} ${!sidebarOpen ? 'close' : ''}`}
          >
            {label}
          </Text>
        </Box>
        {hasLinks && (
          <ChevronIcon
            className={classes.chevron}
            size={14}
            stroke={1.5}
            style={{
              transform: opened ? `rotate(${90}deg)` : 'none',
            }}
          />
        )}
      </Group>
    </UnstyledButton>
  );

  const handleToggleCollapse = (event: FormEvent) => {
    setOpened((o) => !o);
    dispatch(toggleSidebar(menuId as number));
    !sidebarOpen && disableHandleOpen && handleToggleSidebar(event);
  };

  if (hasLinks)
    return (
      <>
        {navItem(true)}
        <Collapse className={classes.collapse} in={opened}>
          {children}
        </Collapse>
      </>
    );

  return link ? (
    <Link className={classes.linkParent} href={link}>
      {navItem()}
    </Link>
  ) : null;
};

export { LinksGroup };
