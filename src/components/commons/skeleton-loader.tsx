import { createStyles } from '@mantine/core';
import React from 'react';

type SkeletonLoader = {
  bgColor?: string;
  height?: number;
  width?: number | string;
  borderRadius?: number;
};

const useStyles = createStyles(
  (theme, { height, bgColor, width, borderRadius }: SkeletonLoader) => ({
    loader: {
      width,
      height,
      paddingBottom: theme.spacing.sm,
      borderRadius: borderRadius ?? theme.radius.lg,
      background: `linear-gradient(270deg, rgba(219, 219, 219, 0.05) 0%, ${bgColor} 50%)`,
      backgroundSize: '1300px',
      animationDuration: '1s',
      animationFillMode: 'forwards',
      animationIterationCount: 'infinite',
      animationTimingFunction: 'linear',
      animationName: 'placeholderAnimate',

      '@keyframes placeholderAnimate': {
        '0%': {
          backgroundPosition: '-650px 0',
        },
        '100%': {
          backgroundPosition: '650px 0',
        },
      },
    },
  }),
);

const SkeletonLoader = ({
  height,
  bgColor,
  width,
  borderRadius,
}: SkeletonLoader) => {
  const { classes } = useStyles({
    width: width || '100%',
    height: height || 16,
    borderRadius: borderRadius || undefined,
    bgColor: bgColor || '#DBDBDB',
  });

  return <div className={classes.loader} />;
};

export default SkeletonLoader;
