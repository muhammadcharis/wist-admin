import {
  createStyles,
  Text,
  Table,
  ScrollArea,
  Card,
  TextInput,
} from '@mantine/core';
import { IconSearch } from '@tabler/icons';
import React, { ChangeEvent, FC, ReactNode, useState } from 'react';
import {
  TableCellType,
  TableHeaderType,
  TdProps,
  ThProps,
} from '@/types/table';

export interface DefaultTableProps {
  title?: string;
  isLoading: boolean;
  dataList: TableCellType[][];
  headerList: TableHeaderType[];
  onSearch?: (e: ChangeEvent<HTMLInputElement>) => void;
  pagination?: ReactNode;
}

const useStyles = createStyles((theme) => ({
  header: {
    position: 'sticky',
    top: 0,
    backgroundColor:
      theme.colorScheme === 'dark' ? theme.colors.dark[7] : theme.white,
    transition: 'box-shadow 150ms ease',

    '&::after': {
      content: '""',
      position: 'absolute',
      left: 0,
      right: 0,
      bottom: 0,
      borderBottom: `1px solid ${
        theme.colorScheme === 'dark'
          ? theme.colors.dark[3]
          : theme.colors.gray[2]
      }`,
    },
  },

  loader: {
    padding: theme.spacing.sm,
    height: '10px',
    borderRadius: theme.radius.lg,
    background:
      'linear-gradient(270deg, rgba(219, 219, 219, 0.05) 0%, #DBDBDB 50%)',
    backgroundSize: '1300px',
    animationDuration: '1s',
    animationFillMode: 'forwards',
    animationIterationCount: 'infinite',
    animationTimingFunction: 'linear',
    animationName: 'placeholderAnimate',

    '@keyframes placeholderAnimate': {
      '0%': {
        backgroundPosition: '-650px 0',
      },
      '100%': {
        backgroundPosition: '650px 0',
      },
    },
  },

  inputSearch: {
    input: {
      border: '0 !important',
      maxWidth: '398px',
    },
  },

  th: {
    padding: '0 !important',
    borderBottom: `1px solid ${theme.colors.gray[2]}`,
    '&:first-of-type > div': {
      paddingLeft: 0,
    },
  },

  td: {
    padding: `${theme.spacing.xs}px 0 !important`,
    '&:first-of-type > div': {
      paddingLeft: 0,
    },
  },

  tr: {
    '&:not(:last-child)': {
      borderBottom: `1px solid ${theme.colors.gray[2]}`,
    },
  },

  control: {
    fontSize: 16,
    width: '100%',
    padding: `${theme.spacing.xs}px ${theme.spacing.md}px`,

    '&:hover': {
      backgroundColor:
        theme.colorScheme === 'dark'
          ? theme.colors.dark[6]
          : theme.colors.gray[0],
    },
  },

  customTd: {
    padding: `${theme.spacing.xs}px ${theme.spacing.md}px`,
    '&:first-of-type > div': {
      paddingLeft: 0,
    },
  },
}));

const BentoTable: FC<DefaultTableProps> = ({
  dataList,
  headerList,
  onSearch,
  isLoading,
  pagination,
}: DefaultTableProps) => {
  const { classes, cx } = useStyles();
  const [scrolled, setScrolled] = useState(false);

  const Th = ({ children, textAlign = 'left' }: ThProps) => {
    return (
      <th className={classes.th}>
        <Text
          align={textAlign}
          className={classes.control}
          weight={700}
          size="sm"
          color="#919398"
        >
          {children}
        </Text>
      </th>
    );
  };

  const Td = ({ children, textAlign = 'left', textColor }: TdProps) => {
    return React.isValidElement(children) ? (
      <td className={classes.customTd}>{children}</td>
    ) : (
      <td className={classes.td}>
        <Text
          align={textAlign}
          className={classes.control}
          weight={400}
          transform="capitalize"
          size="sm"
          color={textColor || '#606266'}
        >
          {children}
        </Text>
      </td>
    );
  };

  return (
    <>
      {onSearch && (
        <TextInput
          className={classes.inputSearch}
          icon={<IconSearch size={18} stroke={1.5} />}
          radius="md"
          size="md"
          onChange={onSearch}
          placeholder="Cari kata kunci"
        />
      )}
      <Card radius="md" p="xl" mt={onSearch ? 'md' : '0'}>
        <ScrollArea
          offsetScrollbars
          type="never"
          sx={{ height: 578 }}
          onScrollPositionChange={({ y }) => setScrolled(y !== 0)}
        >
          <Table
            horizontalSpacing="md"
            verticalSpacing="xs"
            sx={{ minWidth: 700 }}
          >
            <thead className={cx(classes.header)}>
              <tr>
                {headerList.map((header: TableHeaderType, index: number) => (
                  <Th key={index}>{header.title}</Th>
                ))}
              </tr>
            </thead>
            <tbody>
              {isLoading
                ? Array.from(Array(3), (e, key) => (
                    <tr key={key} className={classes.tr}>
                      {Array.from(Array(headerList.length), (e, key) => (
                        <Td key={key}>
                          <div className={classes.loader} />
                        </Td>
                      ))}
                    </tr>
                  ))
                : dataList?.map((data: TableCellType[], index: number) => {
                    return (
                      <tr key={index} className={classes.tr}>
                        {data?.map((_, index2: number) => {
                          const { textAlign, cell, textColor } = data[index2];

                          return (
                            <Td
                              key={index2}
                              textAlign={textAlign}
                              textColor={textColor}
                            >
                              {cell}
                            </Td>
                          );
                        })}
                      </tr>
                    );
                  })}
            </tbody>
          </Table>
        </ScrollArea>
        {pagination}
      </Card>
    </>
  );
};

export default BentoTable;
