import {
  Flex,
  Pagination as Pagination_,
  PaginationProps as PaginationProps_,
} from '@mantine/core';
import React from 'react';
import { renderComponentIf } from '@/utils';

const Pagination = ({ total, page, onChange, ...rest }: PaginationProps_) => {
  return renderComponentIf(total > 1)(
    <Flex justify="flex-end" pr="xl" mt={32}>
      <Pagination_
        total={total}
        page={page}
        onChange={onChange}
        styles={() => ({
          item: {
            '&[aria-disabled]': {
              background: '#E8E8E9',
            },
          },
        })}
        {...rest}
      />
    </Flex>,
  );
};

export default Pagination;
