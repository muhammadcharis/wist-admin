import React from 'react';
import { useSelector } from 'react-redux';
import { getUserAuth } from '@/stores/features/auth/slice';
import {
  Header,
  Text,
  createStyles,
  Group,
  Container,
  Button,
  Popover,
  List,
  Avatar,
} from '@mantine/core';
import Icons from '@/components/commons/icons';

interface AppHeaderProps {
  opened: boolean;
  handleToggleSidebar: () => void;
  onLogout: () => void;
}

const useStyles = createStyles((theme) => ({
  burgerStyle: {
    cursor: 'pointer',

    '&.active': {
      transform: 'rotate(180deg)',
    },
  },
  headerStyle: {
    boxShadow: theme.shadows.xs,
    transition: 'width .5s',

    ['@media (max-width: 767px)']: {
      width: '100%',
      maxWidth: '100%',
      minWidth: '100%',
    },
  },
  inner: {
    width: '100%',
    maxWidth: 2500,
    display: 'flex',
    alignItems: 'center',
    height: '100%',
    justifyContent: 'space-between',
    margin: 0,
  },
  logoutStyle: {
    paddingBottom: 4,
  },
}));

const AppHeader = ({
  opened,
  handleToggleSidebar,
  onLogout,
}: AppHeaderProps) => {
  const { classes } = useStyles();
  const user = useSelector(getUserAuth);

  return (
    <Header
      className={classes.headerStyle}
      left="inherit"
      height={{ base: 51 }}
      w={'100%'}
    >
      <Container className={classes.inner}>
        <Group
          position="apart"
          style={{
            maxWidth: '242px',
            width: '242px',
          }}
        >
          <Icons type="logoWist" width="75px" height="34px" />

          <Icons
            onClick={handleToggleSidebar}
            className={`${classes.burgerStyle} ${opened ? 'active' : ''}`}
            type="hamburger"
            color="#000000"
            width="30px"
            height="30px"
          />
        </Group>
        <Group
          position="right"
          spacing={'xl'}
          style={{
            maxWidth: '242px',
            width: '242px',
          }}
        >
          <Text fw={700}>{user.name}</Text>
          <Popover width={200} position="bottom" withArrow shadow="md">
            <Popover.Target>
              <Avatar component={'button'} radius="xl" />
            </Popover.Target>
            <Popover.Dropdown>
              <List listStyleType={'none'}>
                <Button size="sm" variant="subtle" compact onClick={onLogout}>
                  <span className={classes.logoutStyle}>Logout</span>
                </Button>
              </List>
            </Popover.Dropdown>
          </Popover>
        </Group>
      </Container>
    </Header>
  );
};

export default AppHeader;
