import { Navbar, ScrollArea, createStyles, Text } from '@mantine/core';
import React from 'react';
import { useSelector } from 'react-redux';
import Icons from '@/components/commons/icons';
import { LinksGroup } from '@/components/commons/navbar/links-group';
import { getAllSidebar } from '@/stores/features/sidebar/slice';

const useStyles = createStyles((theme) => ({
  navbar: {
    paddingBottom: 0,
    transition: 'width .3s',
  },

  header: {
    padding: theme.spacing.md,
    paddingTop: 0,
    marginLeft: -theme.spacing.xs,
    marginRight: -theme.spacing.xs,
    color: theme.colorScheme === 'dark' ? theme.white : theme.black,
    borderBottom: `1px solid ${
      theme.colorScheme === 'dark' ? theme.colors.dark[4] : theme.colors.gray[3]
    }`,
  },

  links: {
    marginRight: 0,
  },

  linksInner: {
    paddingBottom: theme.spacing.xl,
  },

  close: {
    cursor: 'pointer',
  },

  closeContainer: {
    display: 'none',
    padding: `${theme.spacing.md}px ${theme.spacing.md}px`,

    ['@media (max-width: 767px)']: {
      display: 'flex',
      width: '100%',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
  },
}));

interface AppSidebarProps {
  opened?: boolean;
  widthSidebar: number;
  shadows?: string;
  handleToggleSidebar: () => void;
}

const AppSidebar = ({
  opened,
  widthSidebar,
  handleToggleSidebar,
  ...others
}: AppSidebarProps) => {
  const { sidebar: listsMenu } = useSelector(getAllSidebar);
  const { classes } = useStyles();

  const links = listsMenu.map((item, index) => {
    return (
      <LinksGroup
        key={index}
        sidebarOpen={opened}
        initiallyOpened={item.initiallyOpened}
        handleToggleSidebar={handleToggleSidebar}
        label={item.name}
        link={item.urlPath as string}
        menuId={item.menuId}
        subMenus={item.children}
      />
    );
  });

  return (
    <Navbar
      hiddenBreakpoint="sm"
      hidden={opened}
      width={{ sm: widthSidebar, lg: widthSidebar }}
      className={classes.navbar}
      {...others}
    >
      <Navbar.Section
        grow
        className={classes.links}
        component={ScrollArea}
        scrollHideDelay={500}
      >
        <div className={classes.linksInner}>{links}</div>
      </Navbar.Section>
    </Navbar>
  );
};

export default AppSidebar;
