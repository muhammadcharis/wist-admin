import React, { MouseEventHandler, useState } from 'react';
import AppHeader from '@/components/layouts//app-header';
import AppSidebar from '@/components/layouts/app-sidebar';
import useRedirect from '@/hooks/libs/use-redirect';
import useLogout from '@/hooks/api/auth/use-logout';
import { useTypedSelector } from '@/hooks/libs/use-type-selector';

import {
  AppShell,
  Text,
  createStyles,
  Container,
  Grid,
  Button,
  Flex,
} from '@mantine/core';
import { IconArrowBack, IconPlus } from '@tabler/icons';
import Head from 'next/head';

type Props = {
  children: React.ReactNode;
  title: string;
  pageTitle?: string;
  backButton?: boolean;
  actionAddButton?: boolean;
  handlerAddButton?: MouseEventHandler;
};

const useStyles = createStyles(() => ({
  pageTitle: {
    fontSize: 24,
    fontWeight: 700,
  },
}));

const AppLayout = ({
  title,
  children,
  pageTitle,
  backButton,
  actionAddButton,
  handlerAddButton,
}: Props) => {
  const accessToken = useTypedSelector(
    (state) => state.reducer.user.accessToken,
  );

  const { classes } = useStyles();
  const [opened, setOpened] = useState(true);
  const [widthSidebar, setWidthSidebar] = useState<number>(250);

  const { refetch: doLogout } = useLogout();

  useRedirect({
    toUrl: '/login',
    condition: !!accessToken === false,
  });

  const handleToggleSidebar: () => void = () => {
    setOpened(!opened);
    setWidthSidebar(!opened ? 250 : 65);
  };

  return (
    <>
      <Head>
        <title>{title} | WIST</title>
      </Head>

      {!!accessToken && (
        <AppShell
          navbarOffsetBreakpoint="sm"
          asideOffsetBreakpoint="sm"
          padding="sm"
          navbar={
            <AppSidebar
              widthSidebar={widthSidebar}
              shadows="sm"
              opened={opened}
              handleToggleSidebar={handleToggleSidebar}
            />
          }
          sx={(theme) => ({
            backgroundColor: theme.colors.gray[0],
          })}
          header={
            <AppHeader
              opened={opened}
              onLogout={doLogout}
              handleToggleSidebar={handleToggleSidebar}
            />
          }
        >
          <Container size={'xl'}>
            <Grid py={8}>
              {pageTitle && (
                <Grid.Col span={6} my={16}>
                  <Flex
                    justify="flex-start"
                    align="center"
                    direction="row"
                    wrap="nowrap"
                    style={{ height: '100%' }}
                  >
                    <Text className={classes.pageTitle}>{pageTitle}</Text>
                  </Flex>
                </Grid.Col>
              )}
              {actionAddButton && (
                <Grid.Col span={6}>
                  (
                  <Flex
                    justify="flex-end"
                    align="center"
                    direction="row"
                    wrap="nowrap"
                  >
                    <Button
                      radius="md"
                      leftIcon={<IconPlus />}
                      onClick={handlerAddButton}
                    >
                      Add New
                    </Button>
                  </Flex>
                  )
                </Grid.Col>
              )}
              {backButton && (
                <Grid.Col span={6}>
                  <Flex
                    justify="flex-end"
                    align="center"
                    direction="row"
                    wrap="nowrap"
                  >
                    <Button
                      leftIcon={<IconArrowBack />}
                      variant="subtle"
                      onClick={() => window.history.go(-1)}
                    >
                      Back
                    </Button>
                  </Flex>
                </Grid.Col>
              )}
              <Grid.Col span={12} mr={0}>
                <Container size={'xl'} p={0}>
                  {children}
                </Container>
              </Grid.Col>
            </Grid>
          </Container>
        </AppShell>
      )}
    </>
  );
};

export default AppLayout;
