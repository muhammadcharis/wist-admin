import { useQuery } from '@tanstack/react-query';
import api from '@/services/api';

const useRefreshToken = () => {
  return useQuery({
    queryKey: ['refreshToken'],
    queryFn: () => api.getRefreshToken(),
    enabled: false,
    retry: false,
  });
};

export default useRefreshToken;
