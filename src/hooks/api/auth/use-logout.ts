import { useQuery } from '@tanstack/react-query';
import api from '@/services/api';
import { useRouter } from 'next/router';
import { useDispatch } from 'react-redux';
import { userLogout } from '@/stores/features/auth/slice';
import { removeMenu } from '@/stores/features/sidebar/slice';

const useLogout = () => {
  const dispatch = useDispatch();
  const router = useRouter();

  return useQuery({
    queryKey: ['refreshToken'],
    queryFn: () => api.getLogout(),
    enabled: false,
    retry: false,
    onSuccess: async () => {
      await Promise.all([dispatch(userLogout()), dispatch(removeMenu())]);

      router.replace('/login');
    },
  });
};

export default useLogout;
