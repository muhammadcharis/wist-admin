import { useQuery } from '@tanstack/react-query';
import useNotificationHook from '@/hooks/libs/use-notification';
import api from '@/services/api';
import { useTypedSelector } from '@/hooks/libs/use-type-selector';
import { AxiosError } from 'axios';

export const useGetProductCategories = () => {
  const notification = useNotificationHook();
  const accessToken = useTypedSelector(
    (state) => state.reducer.user.accessToken,
  );

  return useQuery({
    queryKey: ['getProductCategory'],
    queryFn: () => api.getProductCategory(),
    onError: (err: Error) => {
      console.log(err.message);
      notification.error({
        title: 'error',
        message: err.message,
      });
    },
    enabled: !!accessToken,
    refetchInterval: false,
    refetchOnWindowFocus: false,
    refetchOnReconnect: false,
    refetchOnMount: false,
    retry: false,
  });
};
