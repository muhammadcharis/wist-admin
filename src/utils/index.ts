/* eslint-disable index/only-import-export */
export function renderComponentIf(state: boolean) {
  return (component: JSX.Element) => (state ? component : null);
}

export function arrayIsEmpty(list?: Array<unknown>) {
  if (!list) return true;

  return Array.isArray(list) && list.length === 0;
}
