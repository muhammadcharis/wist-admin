import dayjs from 'dayjs';
import timezone from 'dayjs/plugin/timezone';
import utc from 'dayjs/plugin/utc';
import 'dayjs/locale/id';

type UseOptions = {
  format?: string;
};

type FormatDateProps = {
  date: string | undefined;
  options: UseOptions;
};

const formatDate = ({ date, options = {} }: FormatDateProps) => {
  dayjs.locale('id');
  dayjs.extend(utc);
  dayjs.extend(timezone);

  return dayjs(date).format(options.format ?? 'DD MMMM YYYY');
};

export default formatDate;
