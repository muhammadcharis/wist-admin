import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '@/stores/store';
import { Menu } from '@/types/login';

const initialState: Menu[] = [];

const sidebarSlice = createSlice({
  name: 'sidebar',
  initialState,
  reducers: {
    setSidebar: (state, actions: PayloadAction<Menu[]>) => {
      const { payload } = actions;
      state = payload;
      return state;
    },
    toggleSidebar: (state, action: PayloadAction<number>) => {
      const { payload } = action;
      const editedIndex = state.findIndex((value) => value.menuId === payload);
      state[editedIndex].initiallyOpened = !state[editedIndex].initiallyOpened;
      return state;
    },
    removeMenu: (state) => {
      state = initialState;
      return state;
    },
  },
});

export const getAllSidebar = (state: RootState) => state.reducer;
export const { toggleSidebar, setSidebar, removeMenu } = sidebarSlice.actions;

export default sidebarSlice.reducer;
