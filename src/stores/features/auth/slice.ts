import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '@/stores/store';
import { User, Org } from '@/types/login';
import storage from 'redux-persist/lib/storage';

type UserState = {
  user: User;
  accessToken: string;
  org: Org[];
};

const initialState: UserState = {
  user: {
    userId: undefined,
    orgId: undefined,
    roleId: undefined,
    clientId: undefined,
    email: '',
    username: '',
    name: '',
  },
  accessToken: '',
  org: [],
};

const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setUserAuth: (state, actions: PayloadAction<User>) => {
      const { payload } = actions;
      state.user = payload;
      return state;
    },
    setAccessToken: (state, actions: PayloadAction<string>) => {
      const { payload } = actions;
      state.accessToken = payload;
      return state;
    },
    setUserOrg: (state, actions: PayloadAction<Org[]>) => {
      const { payload } = actions;
      state.org = payload;
      return state;
    },
    userLogout: (state) => {
      storage.removeItem('persist:root');
      state = initialState;
      return state;
    },
  },
});

export const getUserAuth = (state: RootState) => state.reducer.user.user;
export const getAccessToken = (state: RootState) =>
  state.reducer.user.accessToken;
export const getOrg = (state: RootState) => state.reducer.user.org;
export const { setUserAuth, setAccessToken, setUserOrg, userLogout } =
  userSlice.actions;

export default userSlice.reducer;
