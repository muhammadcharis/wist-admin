export interface ProductCategoryItem {
  id: number;
  name: string;
  description: string;
  isActive: boolean;
  orgId: number;
  clientId: number;
  createdAt: string;
  updatedAt: string;
}
