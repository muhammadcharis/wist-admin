import { MantineColor } from '@mantine/core';
import { ReactNode, CSSProperties } from 'react';

export type ThProps = {
  children: React.ReactNode;
  textAlign?: CSSProperties['textAlign'];
};

export type TdProps = {
  children: React.ReactNode;
  textAlign?: CSSProperties['textAlign'];
  textColor?: MantineColor;
};

export type TableHeaderType = {
  title: string;
  key: number | string;
  fixed?: boolean;
  width?: number | string;
};

export type TableCellType = {
  cell: ReactNode | string;
  width?: string;
  textAlign?: CSSProperties['textAlign'];
  textColor?: MantineColor;
};
