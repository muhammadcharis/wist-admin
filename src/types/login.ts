export interface User {
  userId: number | undefined;
  email: string;
  username: string;
  name: string;
  orgId: number | undefined;
  roleId: number | undefined;
  clientId: number | undefined;
}

export interface Menu {
  menuId: number | undefined;
  name: string;
  urlPath: string;
  children: Menu[];
  parentMenuId: number | undefined;
  initiallyOpened: boolean;
}

export interface Org {
  orgId: number | undefined;
  orgName: string;
}
