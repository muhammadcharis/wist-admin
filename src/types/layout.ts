import { MouseEventHandler } from 'react';

export type AppHeaderProps = {
  opened: boolean;
  handlerOpen: MouseEventHandler<HTMLButtonElement> | undefined;
};
