import { TypeIconsProps } from '@/components/commons/icons';

export type ListMenu = {
  label: string;
  initiallyOpened?: boolean;
  subMenus?: SubMenusProps[];
  link?: string;
};

export interface ListMenuIcon extends ListMenu {
  icon: TypeIconsProps;
}

export type SubMenusProps = {
  label: string;
  link: string;
  icon: TypeIconsProps;
};
