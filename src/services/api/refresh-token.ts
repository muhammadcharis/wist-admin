import { REFRESH_TOKEN_PATH } from '@/configs/constants';
import { http } from '@/services/fetcher/axios';

const getRefreshToken = async () => {
  const { data } = await http.get(REFRESH_TOKEN_PATH);
  return data;
};

const refreshTokenServices = {
  getRefreshToken,
};

export default refreshTokenServices;
