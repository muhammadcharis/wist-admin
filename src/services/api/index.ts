import loginServices from '@/services/api/login';
import logoutServices from '@/services/api//logout';
import refreshTokenServices from '@/services/api/refresh-token';
import productCategoryService from '@/services/api/produk-kategori';

const api = {
  ...logoutServices,
  ...loginServices,
  ...refreshTokenServices,
  ...productCategoryService,
};

export default api;
