import { http } from '@/services/fetcher/axios';
import { LOGOUT_PATH } from '@/configs/constants';

const getLogout = async () => {
  const { data } = await http.post(LOGOUT_PATH);
  return data;
};

const logoutServices = {
  getLogout,
};

export default logoutServices;
