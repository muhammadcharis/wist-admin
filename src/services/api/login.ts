import { http } from '@/services/fetcher/axios';
import { LOGIN_PATH } from '@/configs/constants';
import { LoginFormPropsRequest } from '@/hooks/api/auth/use-login';
import { Menu, User, Org } from '@/types/login';

type LoginProps = {
  payload: LoginFormPropsRequest;
};

type UserReponse = {
  userId?: number | undefined;
  email?: string;
  username?: string;
  name?: string;
  Org_id?: number | undefined;
  Role_id?: number | undefined;
  Client_id?: number | undefined;
};

type MenuResponse = {
  Menu_id?: number | undefined;
  Name?: string;
  ParentMenu_id?: number | undefined;
  url_path?: string;
  children?: MenuResponse[];
};

type OrgResponse = {
  Org_id?: number;
  orgname?: string;
};

export type GetLoginResponse = {
  user?: UserReponse;
  menu?: MenuResponse[];
  accessToken?: string;
  org?: OrgResponse[];
};

const mapChildren = (children: MenuResponse[] | undefined): Menu[] => {
  if (Array.isArray(children)) {
    return children.map((child) => {
      return {
        menuId: child.Menu_id ?? undefined,
        name: child.Name ?? '',
        urlPath: child.url_path ?? '',
        children: mapChildren(child.children) ?? [],
        parentMenuId: child.ParentMenu_id ?? undefined,
        initiallyOpened: true,
      };
    });
  } else return [];
};

const map = {
  getAuthFromRemote: (response?: GetLoginResponse) => {
    return {
      user:
        ({
          userId: response?.user?.userId ?? undefined,
          email: response?.user?.email ?? '',
          username: response?.user?.username ?? '',
          name: response?.user?.name ?? '',
          orgId: response?.user?.Org_id ?? undefined,
          roleId: response?.user?.Role_id ?? undefined,
          clientId: response?.user?.Client_id ?? undefined,
        } as User) ?? {},
      menu:
        response?.menu?.map((item) => {
          return {
            menuId: item.Menu_id ?? undefined,
            name: item.Name ?? '',
            urlPath: item.url_path ?? '',
            children: mapChildren(item.children) ?? [],
            parentMenuId: item.ParentMenu_id ?? undefined,
            initiallyOpened: true,
          } as Menu;
        }) ?? [],
      org:
        response?.org?.map((item) => {
          return {
            orgId: item.Org_id ?? undefined,
            orgName: item.orgname ?? '',
          } as Org;
        }) ?? [],
      accessToken: response?.accessToken ?? '',
    };
  },
};

const getLogin = async ({ payload }: LoginProps) => {
  const { data } = await http.post(LOGIN_PATH, payload);
  return map.getAuthFromRemote(data);
};

const loginServices = {
  getLogin,
};

export default loginServices;
