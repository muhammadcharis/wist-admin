import { PRODUCT_CATEGORY_PATH } from '@/configs/constants';
import { http } from '@/services/fetcher/axios';
import { ProductCategoryItem } from '@/types/produk-kategori';

const fallbackDate = new Date(0).toISOString();
export type ProductCategoryResponse = {
  ProductCategories_id?: number;
  name?: string;
  description?: string;
  isactive?: boolean;
  org_id?: number;
  client_id?: number;
  createdAt?: string;
  updatedAt?: string;
};

export type GetProductCategoryResponse = {
  status: string;
  msg: string;
  data: ProductCategoryResponse[];
};

const map = {
  productCategoryFromRemote: (response: GetProductCategoryResponse) => {
    return (
      response.data?.map((item) => {
        return {
          id: item.ProductCategories_id ?? '',
          name: item.name ?? '',
          description: item.description ?? '',
          isActive: item.isactive ?? false,
          orgId: item.org_id ?? undefined,
          clientId: item.client_id ?? undefined,
          createdAt: item.createdAt ?? fallbackDate,
          updatedAt: item.updatedAt ?? fallbackDate,
        } as ProductCategoryItem;
      }) ?? []
    );
  },
};

const getProductCategory = async () => {
  const { data } = await http.get<GetProductCategoryResponse>(
    PRODUCT_CATEGORY_PATH,
  );

  return map.productCategoryFromRemote(data);
};

const productCategoryService = {
  getProductCategory,
};

export default productCategoryService;
