import env from '@/configs/env';
import axios, { AxiosRequestConfig } from 'axios';
import router from 'next/router';
import dayjs from 'dayjs';
import jwt_decode, { JwtPayload } from 'jwt-decode';
import { API_URL, REFRESH_TOKEN_PATH } from '@/configs/constants';
const baseURL = env.apiBaseURL;
import { store } from '@/stores/store';
import { setAccessToken } from '@/stores/features/auth/slice';
import { RootState } from '@/stores/store';

export const http = axios.create({
  baseURL: baseURL,
  withCredentials: true,
});

http.interceptors.request.use(
  async (config: AxiosRequestConfig) => {
    const state: RootState = store.getState();
    const accessToken = state.reducer.user.accessToken;

    if (accessToken) {
      config.headers['Authorization'] = `Bearer ${accessToken}`;
      const decode: JwtPayload = jwt_decode(accessToken);

      if (decode?.exp) {
        const isExpired = dayjs.unix(decode.exp).diff(dayjs()) < 1;
        if (!isExpired) return config;

        const { data } = await axios.get(`${API_URL}${REFRESH_TOKEN_PATH}`, {
          withCredentials: true,
        });

        console.log(data.accessToken);
        store.dispatch(setAccessToken(data.accessToken));

        config.headers['Authorization'] = `Bearer ${data.accessToken}`;
        return config;
      } else return config;
    }

    return config;
  },
  (error) => {
    throw error;
  },
);

http.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    if (error.response.status == 401) {
      //removeAccessToken();
      router.replace('/login');
    }
    return Promise.reject(error);
  },
);

export const defaultQueryFn = async ({ queryKey }: any) => {
  try {
    const { data } = await http.get(queryKey[0], { params: queryKey[1] });
    return data;
  } catch (err) {
    if (axios.isAxiosError(err)) throw err.response;
    throw err;
  }
};

export { axios };
