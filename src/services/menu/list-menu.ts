import { IconWallet, IconGauge } from '@tabler/icons';
import { ListMenuIcon } from '@/types/sidebar';

export const listsMenu: ListMenuIcon[] = [
  {
    label: 'Dashboard',
    icon: IconGauge,
    initiallyOpened: true,
    links: [{ label: 'Home', link: '/' }],
  },

  {
    label: 'Billing',
    icon: IconWallet,
    initiallyOpened: true,
    links: [
      { label: 'Billing Seller', link: '/billing-seller' },
      { label: 'Plan Package', link: '/plan-package' },
    ],
  },
];
